import 'package:flutter/material.dart';

class Review extends StatelessWidget {

  String pathImage = 'assets/img/iam.jpg';
  String name = "Carlos alberto";
  String details = "2 review";
  String comment = "mmlo el miiiio ";

  Review(this.pathImage, this.name, this.details, this.comment);

  @override
  Widget build(BuildContext context){

    final photo = Container(
      margin: EdgeInsets.only(
        top: 20.0,
        left: 20.0
      ),
      width: 80.0,
      height: 80.0,

      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(image: AssetImage(pathImage), fit: BoxFit.cover)
      ),
    );


  final userComment = Container(
    margin: EdgeInsets.only(
      left: 20.0
    ),
    child: Text(
      details,
      textAlign: TextAlign.left,
      style: TextStyle(
        fontFamily: 'Lato',
        fontSize: 13.0,
        fontWeight: FontWeight.w900
      ),
    ),
  );

  final userInfo = Container(
    margin: EdgeInsets.only(
      left: 20.0
    ),
    child: Text(
      details,
      textAlign: TextAlign.left,
      style: TextStyle(
        fontFamily: 'Lato',
        fontSize: 13.0,
        color: Color(0xFFa3a5a7)
      ),
    ),
  );

  final username = Container(
    margin: EdgeInsets.only(
      left: 20.0
    ),
    child: Text(
      name,
      textAlign: TextAlign.left,
      style: TextStyle(
        fontFamily: 'Lato',
        fontSize: 17.0
      ),
    ),
  );

  final userDetails = Column(
    crossAxisAlignment: CrossAxisAlignment.start, //para alinear al inicio porque por defecto es centrado
    children: <Widget>[
      username,
      userInfo,
      userComment
      ],
  );

    return Row(children: <Widget>[
      photo,
      userDetails
    ],);
  } 
}